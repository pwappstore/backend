REGISTRY_ID=$(shell grep gitlab.com .git/config|sed 's/url = https:\/\//registry./g'|sed -e "s/\.git$$//g"|xargs)
BRANCH=$(shell git symbolic-ref --short -q HEAD|sed -e 's/master$$/latest/g')
VERSION=`git rev-parse --short HEAD`

build:
	docker build . -t $(REGISTRY_ID):$(BRANCH)

run: build
	docker run $(REGISTRY_ID):$(BRANCH)

push: build
	docker push $(REGISTRY_ID):$(BRANCH)
